# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.21.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.21.0) (2022-02-07)


### Features

* **dev:** add new feature ([55eac87](https://gitlab.com/Naughty1905/changelog-settings/commit/55eac87f378cf646f7c46d0c3c6f25bece15e08c))
* **dev:** add new feature ([c37bfe6](https://gitlab.com/Naughty1905/changelog-settings/commit/c37bfe6da0e5b6fc8609622e50e9714ebfb80b6b))
* **dev:** add new feature ([b2e6f6b](https://gitlab.com/Naughty1905/changelog-settings/commit/b2e6f6ba39126268d6765d738a36d1838abae822))
* **dev:** add old features ([2a4996e](https://gitlab.com/Naughty1905/changelog-settings/commit/2a4996eb1db7aa9c29d69bcca00737a7fd70275a))
* **dev:** new feature ([3b3fe5b](https://gitlab.com/Naughty1905/changelog-settings/commit/3b3fe5b1a2725165ede3cf2d87efb25c302e11d8))


### Bug Fixes

* **dev:** add something ([38c4226](https://gitlab.com/Naughty1905/changelog-settings/commit/38c42261df712b025753d1abe2c1489d50c0580e))

## [1.20.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.20.0) (2021-10-06)


### Features

* **dev:** add new feature ([55eac87](https://gitlab.com/Naughty1905/changelog-settings/commit/55eac87f378cf646f7c46d0c3c6f25bece15e08c))
* **dev:** add new feature ([c37bfe6](https://gitlab.com/Naughty1905/changelog-settings/commit/c37bfe6da0e5b6fc8609622e50e9714ebfb80b6b))
* **dev:** add new feature ([b2e6f6b](https://gitlab.com/Naughty1905/changelog-settings/commit/b2e6f6ba39126268d6765d738a36d1838abae822))
* **dev:** new feature ([3b3fe5b](https://gitlab.com/Naughty1905/changelog-settings/commit/3b3fe5b1a2725165ede3cf2d87efb25c302e11d8))

## [1.19.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.19.0) (2021-10-06)


### Features

* **dev:** add new feature ([55eac87](https://gitlab.com/Naughty1905/changelog-settings/commit/55eac87f378cf646f7c46d0c3c6f25bece15e08c))
* **dev:** add new feature ([c37bfe6](https://gitlab.com/Naughty1905/changelog-settings/commit/c37bfe6da0e5b6fc8609622e50e9714ebfb80b6b))
* **dev:** add new feature ([b2e6f6b](https://gitlab.com/Naughty1905/changelog-settings/commit/b2e6f6ba39126268d6765d738a36d1838abae822))
* **dev:** new feature ([3b3fe5b](https://gitlab.com/Naughty1905/changelog-settings/commit/3b3fe5b1a2725165ede3cf2d87efb25c302e11d8))

## [1.18.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.18.0) (2021-10-06)


### Features

* **dev:** add new feature ([55eac87](https://gitlab.com/Naughty1905/changelog-settings/commit/55eac87f378cf646f7c46d0c3c6f25bece15e08c))
* **dev:** add new feature ([c37bfe6](https://gitlab.com/Naughty1905/changelog-settings/commit/c37bfe6da0e5b6fc8609622e50e9714ebfb80b6b))
* **dev:** add new feature ([b2e6f6b](https://gitlab.com/Naughty1905/changelog-settings/commit/b2e6f6ba39126268d6765d738a36d1838abae822))
* **dev:** new feature ([3b3fe5b](https://gitlab.com/Naughty1905/changelog-settings/commit/3b3fe5b1a2725165ede3cf2d87efb25c302e11d8))

## [1.17.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.17.0) (2021-10-06)


### Features

* **dev:** add new feature ([55eac87](https://gitlab.com/Naughty1905/changelog-settings/commit/55eac87f378cf646f7c46d0c3c6f25bece15e08c))
* **dev:** add new feature ([c37bfe6](https://gitlab.com/Naughty1905/changelog-settings/commit/c37bfe6da0e5b6fc8609622e50e9714ebfb80b6b))
* **dev:** add new feature ([b2e6f6b](https://gitlab.com/Naughty1905/changelog-settings/commit/b2e6f6ba39126268d6765d738a36d1838abae822))
* **dev:** new feature ([3b3fe5b](https://gitlab.com/Naughty1905/changelog-settings/commit/3b3fe5b1a2725165ede3cf2d87efb25c302e11d8))

### [1.16.6](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.16.6) (2021-10-06)

### [1.16.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.16.5) (2021-10-06)

### [1.16.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.16.4) (2021-10-06)

### [1.16.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.2...v1.16.3) (2021-10-06)

### [1.16.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.1...v1.16.2) (2021-10-06)

### [1.16.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.16.0...v1.16.1) (2021-10-06)

## [1.16.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.7...v1.16.0) (2021-10-06)


### Features

* **dev:** add new feature ([c2c437f](https://gitlab.com/Naughty1905/changelog-settings/commit/c2c437f9c963d8719dfed355fd20316c08838390))
* **dev:** add new feature ([836e037](https://gitlab.com/Naughty1905/changelog-settings/commit/836e03746da65e69e712d1c4bc53c4993cbcd49c))
* **dev:** add new feature ([59a3b20](https://gitlab.com/Naughty1905/changelog-settings/commit/59a3b207b6d7aefeb5002a6c277383cc396e1ab6))
* **dev:** new feature ([ebf8f9f](https://gitlab.com/Naughty1905/changelog-settings/commit/ebf8f9fa37060db83de7e651a30b388044815653))
* **dev:** new feature ([43eb734](https://gitlab.com/Naughty1905/changelog-settings/commit/43eb73466903c112e983b8a7680a67dfc36c4b3c))
* **dev:** new feature ([b157548](https://gitlab.com/Naughty1905/changelog-settings/commit/b1575483ec8ae36f9c8d349270c01e4e206fa998))

### [1.15.8](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.7...v1.15.8) (2021-10-06)

### [1.15.7](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.6...v1.15.7) (2021-10-06)

### [1.15.6](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.5...v1.15.6) (2021-10-06)

### [1.15.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.4...v1.15.5) (2021-10-06)

### [1.15.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.3...v1.15.4) (2021-10-06)

### [1.15.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.2...v1.15.3) (2021-10-06)

### [1.15.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.1...v1.15.2) (2021-10-06)

### [1.15.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.15.0...v1.15.1) (2021-09-08)

## [1.15.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.27...v1.15.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([1b5b1d7](https://gitlab.com/Naughty1905/changelog-settings/commit/1b5b1d7e5b84033bf706847a6af263a974af8c67))
* **dev:** add gitlab ci rules ([2228374](https://gitlab.com/Naughty1905/changelog-settings/commit/222837476e74cd538aef26872b6f33583b965458))
* **dev:** add gitlab ci rules ([6f1345f](https://gitlab.com/Naughty1905/changelog-settings/commit/6f1345f0242582782566f8be44adf18d30d2d632))
* **dev:** add gitlab ci rules ([604f1f3](https://gitlab.com/Naughty1905/changelog-settings/commit/604f1f3a5ad1b85c94ba7c9cbb4f3b61f087f6b5))

### [1.14.27](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.26...v1.14.27) (2021-09-08)

### [1.14.26](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.25...v1.14.26) (2021-09-08)

### [1.14.25](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.24...v1.14.25) (2021-09-08)

### [1.14.24](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.23...v1.14.24) (2021-09-08)

### [1.14.23](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.22...v1.14.23) (2021-09-08)

### [1.14.22](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.21...v1.14.22) (2021-09-08)

### [1.14.21](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.20...v1.14.21) (2021-09-08)

### [1.14.20](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.19...v1.14.20) (2021-09-08)

### [1.14.19](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.18...v1.14.19) (2021-09-08)

### [1.14.18](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.17...v1.14.18) (2021-09-08)

### [1.14.17](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.16...v1.14.17) (2021-09-08)

### [1.14.16](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.15...v1.14.16) (2021-09-08)

### [1.14.15](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.14...v1.14.15) (2021-09-08)

### [1.14.14](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.13...v1.14.14) (2021-09-08)

### [1.14.13](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.12...v1.14.13) (2021-09-08)

### [1.14.12](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.11...v1.14.12) (2021-09-08)

### [1.14.11](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.10...v1.14.11) (2021-09-08)

### [1.14.10](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.9...v1.14.10) (2021-09-08)

### [1.14.9](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.8...v1.14.9) (2021-09-08)

### [1.14.8](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.7...v1.14.8) (2021-09-08)

### [1.14.7](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.6...v1.14.7) (2021-09-08)

### [1.14.6](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.5...v1.14.6) (2021-09-08)

### [1.14.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.4...v1.14.5) (2021-09-08)

### [1.14.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.3...v1.14.4) (2021-09-08)

### [1.14.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.2...v1.14.3) (2021-09-08)

### [1.14.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.1...v1.14.2) (2021-09-08)

### [1.14.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.14.0...v1.14.1) (2021-09-08)

## [1.14.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.13.2...v1.14.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([dff256e](https://gitlab.com/Naughty1905/changelog-settings/commit/dff256e9ac34539839cb5016d1c54018d1a36159))
* **dev:** add gitlab ci rules ([6986ba7](https://gitlab.com/Naughty1905/changelog-settings/commit/6986ba7e623d9382ce5a7a8f584d9961aba41d19))
* **dev:** add gitlab ci rules ([2d63e2b](https://gitlab.com/Naughty1905/changelog-settings/commit/2d63e2b47df489ed1d9d2a17d866ef50ff6a836e))

### [1.13.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.13.1...v1.13.2) (2021-09-08)

### [1.13.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.13.0...v1.13.1) (2021-09-08)

## [1.13.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.5...v1.13.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([aea93de](https://gitlab.com/Naughty1905/changelog-settings/commit/aea93de837cae570358dadb18f615a5cad1ae6b6))

### [1.12.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.4...v1.12.5) (2021-09-08)

### [1.12.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.3...v1.12.4) (2021-09-08)

### [1.12.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.2...v1.12.3) (2021-09-08)

### [1.12.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.1...v1.12.2) (2021-09-08)

### [1.12.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.12.0...v1.12.1) (2021-09-08)

## [1.12.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.11.0...v1.12.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([984a7c9](https://gitlab.com/Naughty1905/changelog-settings/commit/984a7c929e1f979aa49d9b8e135bb5b606512e92))
* **dev:** add gitlab ci rules ([59b9ee4](https://gitlab.com/Naughty1905/changelog-settings/commit/59b9ee42629856042b403296a77cac22ad3d8539))
* **dev:** add gitlab ci rules ([dabb20a](https://gitlab.com/Naughty1905/changelog-settings/commit/dabb20a2b2896dffa9d43069fda7abf1c169361b))

## [1.11.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.8...v1.11.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([a78a13f](https://gitlab.com/Naughty1905/changelog-settings/commit/a78a13f5ae7cb3840d60b0788e94650a0ff19540))
* **dev:** add gitlab ci rules ([5fa3fae](https://gitlab.com/Naughty1905/changelog-settings/commit/5fa3fae4ebd1d625d6caa49fc1d4f2e3c402b781))
* **dev:** add gitlab ci rules ([e2d8dba](https://gitlab.com/Naughty1905/changelog-settings/commit/e2d8dba0d0f8371559f268139212274d985e3b39))
* **dev:** add gitlab ci rules ([b17e45e](https://gitlab.com/Naughty1905/changelog-settings/commit/b17e45e45e8ff726eeb19128404b4f4bcc9902c1))
* **dev:** add gitlab ci rules ([5bab59d](https://gitlab.com/Naughty1905/changelog-settings/commit/5bab59d18deeee2e57d61ec77d26852914e31c04))
* **dev:** add gitlab ci rules ([9eea314](https://gitlab.com/Naughty1905/changelog-settings/commit/9eea314147bb77f8f7d2336e866305ceb8e82228))
* **dev:** add gitlab ci rules ([9421d30](https://gitlab.com/Naughty1905/changelog-settings/commit/9421d30b68489b32526f670fca92b22261e5f94b))
* **dev:** add gitlab ci rules ([f59f60d](https://gitlab.com/Naughty1905/changelog-settings/commit/f59f60d3a6f2aec48d67d9e5fd9da934e153d669))
* **dev:** add gitlab ci rules ([8c03cd2](https://gitlab.com/Naughty1905/changelog-settings/commit/8c03cd2e2d8abd5413d2f5b56afd42ba8c39de51))
* **dev:** add gitlab ci rules ([06633ff](https://gitlab.com/Naughty1905/changelog-settings/commit/06633ffff7a9a87f05d970321183a18038f92816))
* **dev:** add gitlab ci rules ([c6f2038](https://gitlab.com/Naughty1905/changelog-settings/commit/c6f2038567140f3b7db1a0113e8b5a7ef657a309))
* **dev:** add gitlab ci rules ([a941c5b](https://gitlab.com/Naughty1905/changelog-settings/commit/a941c5baec4855064f4cfcf7ac89dba113d529b2))

### [1.10.8](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.7...v1.10.8) (2021-09-08)

### [1.10.7](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.6...v1.10.7) (2021-09-08)

### [1.10.6](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.5...v1.10.6) (2021-09-08)

### [1.10.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.4...v1.10.5) (2021-09-08)

### [1.10.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.3...v1.10.4) (2021-09-08)

### [1.10.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.2...v1.10.3) (2021-09-08)

### [1.10.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.1...v1.10.2) (2021-09-08)

### [1.10.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.10.0...v1.10.1) (2021-09-08)

## [1.10.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.33...v1.10.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([df5f764](https://gitlab.com/Naughty1905/changelog-settings/commit/df5f76484774eb33fec9cd247bc8fc3c65383552))
* **dev:** add gitlab ci rules ([55f8357](https://gitlab.com/Naughty1905/changelog-settings/commit/55f8357155bc2573cf3b8130d278513b62a31374))

### [1.9.33](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.32...v1.9.33) (2021-09-08)

### [1.9.32](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.31...v1.9.32) (2021-09-08)

### [1.9.31](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.30...v1.9.31) (2021-09-08)

### [1.9.30](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.29...v1.9.30) (2021-09-08)

### [1.9.29](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.28...v1.9.29) (2021-09-08)

### [1.9.28](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.27...v1.9.28) (2021-09-08)

### [1.9.27](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.26...v1.9.27) (2021-09-08)

### [1.9.26](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.25...v1.9.26) (2021-09-08)

### [1.9.25](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.24...v1.9.25) (2021-09-08)

### [1.9.24](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.23...v1.9.24) (2021-09-08)

### [1.9.23](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.22...v1.9.23) (2021-09-08)

### [1.9.22](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.21...v1.9.22) (2021-09-08)

### [1.9.21](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.20...v1.9.21) (2021-09-08)

### [1.9.20](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.19...v1.9.20) (2021-09-08)

### [1.9.19](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.18...v1.9.19) (2021-09-08)

### [1.9.18](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.17...v1.9.18) (2021-09-08)

### [1.9.17](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.16...v1.9.17) (2021-09-08)

### [1.9.16](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.15...v1.9.16) (2021-09-08)

### [1.9.15](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.14...v1.9.15) (2021-09-08)

### [1.9.14](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.13...v1.9.14) (2021-09-08)

### [1.9.13](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.12...v1.9.13) (2021-09-08)

### [1.9.12](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.11...v1.9.12) (2021-09-08)

### [1.9.11](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.10...v1.9.11) (2021-09-08)

### [1.9.10](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.9...v1.9.10) (2021-09-08)

### [1.9.9](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.8...v1.9.9) (2021-09-08)

### [1.9.8](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.7...v1.9.8) (2021-09-08)

### [1.9.7](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.6...v1.9.7) (2021-09-08)

### [1.9.6](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.5...v1.9.6) (2021-09-08)

### [1.9.5](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.4...v1.9.5) (2021-09-08)

### [1.9.4](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.3...v1.9.4) (2021-09-08)

### [1.9.3](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.2...v1.9.3) (2021-09-08)

### [1.9.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.1...v1.9.2) (2021-09-08)

### [1.9.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.9.0...v1.9.1) (2021-09-08)

## [1.9.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.8.0...v1.9.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([9eadded](https://gitlab.com/Naughty1905/changelog-settings/commit/9eaddedcae1a67d906174d70c93a3f04f195ba6a))
* **dev:** add gitlab ci rules ([40a4eb6](https://gitlab.com/Naughty1905/changelog-settings/commit/40a4eb6b680573b76e2cc3f8eae1ae015366bf51))
* **dev:** add gitlab ci rules ([1e30af2](https://gitlab.com/Naughty1905/changelog-settings/commit/1e30af24db9908e7a38c59213d0a80adf79ebb7c))
* **dev:** add gitlab ci rules ([10e5ed2](https://gitlab.com/Naughty1905/changelog-settings/commit/10e5ed2002750ff9fe233cd68882479d2d14cb69))
* **dev:** add gitlab ci rules ([4a1b642](https://gitlab.com/Naughty1905/changelog-settings/commit/4a1b642c43c680aac8f023a47a4f562178ea14fe))

## [1.8.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.7.1...v1.8.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([cc293cb](https://gitlab.com/Naughty1905/changelog-settings/commit/cc293cb92e660359c0a1511e24e7cc78c9e0a9bc))

### [1.7.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.7.0...v1.7.1) (2021-09-08)

## [1.7.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.6.2...v1.7.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([0b6bd97](https://gitlab.com/Naughty1905/changelog-settings/commit/0b6bd97a0db38c12c6fdf8dd206f756caf14c274))

### [1.6.2](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.6.1...v1.6.2) (2021-09-08)

### [1.6.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.6.0...v1.6.1) (2021-09-08)

## [1.6.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.5.0...v1.6.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([4f46d8d](https://gitlab.com/Naughty1905/changelog-settings/commit/4f46d8d7d72bb461240e6ff5b2861573395b2722))
* **dev:** add gitlab ci rules ([9b6c4b3](https://gitlab.com/Naughty1905/changelog-settings/commit/9b6c4b343424d5a1bf38db2acf11eea2171f0267))
* **dev:** add gitlab ci rules ([e02eb73](https://gitlab.com/Naughty1905/changelog-settings/commit/e02eb73667a1717e7fe0917d231395e54d9c1f88))

## [1.5.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.4.0...v1.5.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([813b3f7](https://gitlab.com/Naughty1905/changelog-settings/commit/813b3f75e72e19930fa6bd0eae4111740f3af416))
* **dev:** add gitlab ci rules ([30317de](https://gitlab.com/Naughty1905/changelog-settings/commit/30317de908c4821bca886ea21bb635964ae0277f))
* **dev:** add gitlab ci rules ([64ee315](https://gitlab.com/Naughty1905/changelog-settings/commit/64ee3151323d58af6e4243fd2a347a5ac201c06c))
* **dev:** add gitlab ci rules ([c046688](https://gitlab.com/Naughty1905/changelog-settings/commit/c046688275e80a328230b9e512b081a8f34f03bf))
* **dev:** add gitlab ci rules ([45309d7](https://gitlab.com/Naughty1905/changelog-settings/commit/45309d73c6728368daba597e6cd2ba3b7da1043e))
* **dev:** add gitlab ci rules ([6175137](https://gitlab.com/Naughty1905/changelog-settings/commit/61751370ddf0c733e03a7ff48acbafa1054d1ddc))
* **dev:** add gitlab ci rules ([2ddcb09](https://gitlab.com/Naughty1905/changelog-settings/commit/2ddcb091f07383f13431a3950711c7201fef7ee6))
* **dev:** add gitlab ci rules ([e291c70](https://gitlab.com/Naughty1905/changelog-settings/commit/e291c709438f6740b9024771cdf00e8c0728e9eb))
* **dev:** add gitlab ci rules ([9350bf5](https://gitlab.com/Naughty1905/changelog-settings/commit/9350bf59b6790128bb24f036556632a958b7b7bf))
* **dev:** add gitlab ci rules ([b08ebb5](https://gitlab.com/Naughty1905/changelog-settings/commit/b08ebb5af2cfd268426f90e1280ac453f964956a))
* **dev:** add gitlab ci rules ([0083a66](https://gitlab.com/Naughty1905/changelog-settings/commit/0083a66026e509f2fe03452c0d3bec3907bcf82d))
* **dev:** add gitlab ci rules ([d90a107](https://gitlab.com/Naughty1905/changelog-settings/commit/d90a1078912b5c7b3877de6c88a91a2b1558b0d8))

## [1.4.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.3.0...v1.4.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([cdb4db7](https://gitlab.com/Naughty1905/changelog-settings/commit/cdb4db78c4e8a40375e6de38e7de1d04ede6db21))
* **dev:** add gitlab ci rules ([eb4adf6](https://gitlab.com/Naughty1905/changelog-settings/commit/eb4adf6df4cfed9e4f78283cd96e565253ef7078))
* **dev:** add gitlab ci rules ([949c11b](https://gitlab.com/Naughty1905/changelog-settings/commit/949c11b2a8f26d1edbb8f9118f7266c562b15c4d))
* **dev:** add gitlab ci rules ([639e941](https://gitlab.com/Naughty1905/changelog-settings/commit/639e941d387502bf954b7b73a648e30f31066caa))
* **dev:** add gitlab ci rules ([037052c](https://gitlab.com/Naughty1905/changelog-settings/commit/037052c12a57aeab2978ec9ea129b41d46355cba))

## [1.3.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.2.1...v1.3.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([3bf981c](https://gitlab.com/Naughty1905/changelog-settings/commit/3bf981c25b4016958720b456a223c394988ae659))
* **dev:** add gitlab ci rules ([1097a2c](https://gitlab.com/Naughty1905/changelog-settings/commit/1097a2c1e2919b0670c3bba4fb4b6af9fe3382a0))
* **dev:** add gitlab ci rules ([94d6c83](https://gitlab.com/Naughty1905/changelog-settings/commit/94d6c83f3635a50fb03b03bd3e2d838c66f95222))
* **dev:** add gitlab ci rules ([3cce080](https://gitlab.com/Naughty1905/changelog-settings/commit/3cce080f9abcc8843ea534fe46ea2be2599fa098))

### [1.2.1](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.2.0...v1.2.1) (2021-09-08)

## [1.2.0](https://gitlab.com/Naughty1905/changelog-settings/compare/v1.1.0...v1.2.0) (2021-09-08)


### Features

* **dev:** add gitlab ci rules ([e62fc89](https://gitlab.com/Naughty1905/changelog-settings/commit/e62fc893372601db1d6eb327a4724678b4aeff4f))
* **dev:** add gitlab ci rules ([d6e614b](https://gitlab.com/Naughty1905/changelog-settings/commit/d6e614b95e0d267bb5767fcddc24c396ce5fcd11))
* **dev:** add gitlab ci rules ([903898f](https://gitlab.com/Naughty1905/changelog-settings/commit/903898ff687de635f8ef211bdcc0c7f8ef241ecb))
* **dev:** add gitlab ci rules ([3f0e8f4](https://gitlab.com/Naughty1905/changelog-settings/commit/3f0e8f4234a3c531c689f9d5450df0eb915e2496))
* **dev:** add gitlab ci rules ([dc82dd6](https://gitlab.com/Naughty1905/changelog-settings/commit/dc82dd60f89adc88db3b9a4d05b4f320b691648d))
* **dev:** add gitlab ci rules ([e219b4d](https://gitlab.com/Naughty1905/changelog-settings/commit/e219b4d12d91965ec346899ec5a480505326d5d2))
* **dev:** add gitlab ci rules ([e295d6c](https://gitlab.com/Naughty1905/changelog-settings/commit/e295d6c49dd257f4dccdfcb15146b782e5815407))
* **dev:** add gitlab ci rules ([0eccdb7](https://gitlab.com/Naughty1905/changelog-settings/commit/0eccdb72d2f72f18e0ff68f5c60d058f42225586))
* **dev:** add gitlab ci rules ([f89ae22](https://gitlab.com/Naughty1905/changelog-settings/commit/f89ae22afd599942de0339b86c1f1f935841a758))
* **dev:** add gitlab ci rules ([e11219b](https://gitlab.com/Naughty1905/changelog-settings/commit/e11219b86ce3878dbd628c3dfaad1464e5817e60))
* **dev:** add gitlab ci rules ([e29daf8](https://gitlab.com/Naughty1905/changelog-settings/commit/e29daf8fa61c7d8a52cba7a81c1fe2da345bd523))
* **dev:** add gitlab ci rules ([76020b7](https://gitlab.com/Naughty1905/changelog-settings/commit/76020b779d8b0826bec8a82509577aaae3875880))
* **dev:** add gitlab ci rules ([30289de](https://gitlab.com/Naughty1905/changelog-settings/commit/30289de0b4375d269ebeff3529bff62c43b7b7c5))
* **dev:** add gitlab ci rules ([abbc125](https://gitlab.com/Naughty1905/changelog-settings/commit/abbc1254b7f6951ef528f3dd301b64a1b18f1bc7))
* **dev:** add gitlab ci rules ([de3c7e2](https://gitlab.com/Naughty1905/changelog-settings/commit/de3c7e2b1663bf771bd54d135be2205530998f85))
* **dev:** add gitlab ci rules ([a954dbf](https://gitlab.com/Naughty1905/changelog-settings/commit/a954dbf46d5abb65a4ed1ac7d911dd40b98fdfa7))

## 1.1.0 (2021-09-02)


### Features

* **commitlint:** add commitlint and precommit ([636b359](https://gitlab.com/Naughty1905/changelog-settings/commit/636b359cb1c6585e24d8031aef9249db73442bfa))
* **commitlint:** add commitlint and precommit ([0be8ffb](https://gitlab.com/Naughty1905/changelog-settings/commit/0be8ffbf0f76f710c969f8c1317a1549d237ffd3))
* **configs:** some configs ([339a4f2](https://gitlab.com/Naughty1905/changelog-settings/commit/339a4f2b516bc0499360793e3988ef7b072f7e68))
